<!DOCTYPE html>
<?php
  include 'php/connexion.php';
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
      <link rel="stylesheet" href="css/style.css">
    <title>recapStock</title>
  </head>
  <body>
    <h1>Tableau récapitulatif</h1>
    <div class="recherche">
      <input type="text" name="recherche" id="recherche" value="" placeholder="Search">
      <button type="button" id="buttonSearch" name="button">Rechercher</button>
    </div>
    <button type="button" name="button" onclick="ajoutStock()">Ajoute un stock</button>
    <ul id="groupe"></ul>
    <table id="tabContent">
      <thead>
        <tr>
          <th>Nom de l'article</th>
          <th>Quantités</th>
          <th>prix unitaire</th>
          <th>prix total</th>
          <th>Reférence produit</th>
        </tr>
      </thead>
      <tbody id="tableau">
          <?php while (  $donnees = $recapArticle->fetch()) { ?>
          <tr onclick="msgbox(this)">
            <td><?php echo $donnees['nom_article']; ?></td>

            <td><?php
            if ($donnees['quantite_total']==NULL) {
              echo"A DEFINIR";
            }else {
              echo $donnees['quantite_total'];
            }
            ?></td>
            <td><?php
            if ($donnees['prix_unitaire']==NULL) {
              echo"A DEFINIR";
            }else {
              echo $donnees['prix_unitaire'];
            }
            ?></td>
            <td><?php
            if ($donnees['prix_unitaire']==NULL || $donnees['quantite_total'] ==NULL) {
              echo"A DEFINIR";
            }else {
               echo $donnees['prix_unitaire']*$donnees['quantite_total'];
            }

             ?></td>
             <td><?php
             if ($donnees['ref_produit']==NULL) {
               echo"A DEFINIR";
             }else {
                echo $donnees['ref_produit'];
             }

              ?></td>
             <input type="hidden" class="id" name="id" value="<?php echo $donnees['id_article'] ?>">
            <?php $tot= $tot+$donnees['prix_unitaire']*$donnees['quantite_total'];?>
          </tr>
          <?php } ?>
      </tbody>
    </table>
    <span>Le cout total du stock est de <?php echo $tot; ?> euros</span>
    <a href="index.php">page des codeBar</a>
    <div class="modal">
      <div class="modal-content">
          <span class="close">&#10006;</span>
          <div class="article">
            <label for="Nom_produit">Nom du produit</label>
            <input type="text" name="nom_produit" id="nom_produit" value=""><br>
            <label for="prix_produit">Prix unitaire</label>
            <input type="text" name="prix_produit" id="prix_produit" value=""><br>
            <label for="totalStock">totalStock</label>
            <input type="text" name="totalStock" id="totalStock" value=""><br>
            <label for="codeBar">Référence Article</label>
            <input type="text" name="codeBar" id="codeBar" value="">
            <input type="hidden" name="id" id="id" value="">
            <br>
            <br>
            <input type="button" id="save" name="save" value="Sauvgarder les modifications">
          </div>
      </div>
    </div>
    <div class="modal">
      <div class="modal-content">
          <span class="close">&#10006;</span>
          <h2>Nouveau Stock</h2>
          <label for="nom_stock">Nom du stock</label>
          <input type="text" name="nom_stock" id="nom_stock" value=""><br>
          <input type="button"  id="ajoutGroupe"   name="ajout_groupe" value="Ajouter groupe">
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script/scriptRecap.js" charset="utf-8"></script>
  </body>
</html>
