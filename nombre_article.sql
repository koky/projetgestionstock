DELIMITER | -- On change le délimiteur
CREATE PROCEDURE QtDansStock(OUT total INT)
    -- pas de paramètres dans les parenthèses
BEGIN
  SELECT SUM(`qt_articles`) AS nombre_total, `nom_article` FROM `ligne_commande`
  JOIN article ON ligne_commande.id_article = article.id_article
  GROUP BY `nom_article`
END|
