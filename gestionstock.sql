-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 14 fév. 2019 à 16:47
-- Version du serveur :  10.1.37-MariaDB
-- Version de PHP :  7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestionstock`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id_article` int(11) NOT NULL,
  `nom_article` varchar(50) NOT NULL,
  `prix_unitaire` float DEFAULT NULL,
  `ref_produit` varchar(50) DEFAULT NULL,
  `id_rayon` int(11) DEFAULT NULL,
  `id_rack` int(11) DEFAULT NULL,
  `id_projet` int(11) DEFAULT '1',
  `quantite_total` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id_article`, `nom_article`, `prix_unitaire`, `ref_produit`, `id_rayon`, `id_rack`, `id_projet`, `quantite_total`) VALUES
(1, '32MX795F512LT80I composants SAC CGX', 11.25, NULL, NULL, NULL, 1, 1.5),
(2, 'aero vd black 400ml', 9.13, NULL, NULL, NULL, 1, 6),
(3, 'Adaptateur ssd/hdd 2,5/3,5 startech. Com', 0, '$NCSQ1901020904', NULL, NULL, 1, 13),
(4, 'ADHESIF ANTI VIBRATION 10X10X3', 27.54, NULL, NULL, NULL, 1, 2),
(5, 'Alim ATX 250W pour rack 2U', 45, NULL, NULL, NULL, 1, 14),
(6, 'alim pc divers', 30, NULL, NULL, NULL, 1, 60),
(7, 'alim 5V 1A', 3.49, NULL, NULL, NULL, 1, 100),
(8, 'ALIM OPENFRAME ACE-870A ', 25, NULL, NULL, NULL, 1, 5),
(9, 'Alimentation ATX Be-Quiet 500W', 65, '$NCSQ1901020904', NULL, NULL, 1, 1),
(10, 'ALIMENTATION POUR CARTE KINO-DH610', 23, '$3410280078397', NULL, NULL, 1, 6),
(11, 'ARDUINO PROTO SHIELD REV3 ASSEMBLED', 13.05, NULL, NULL, NULL, 1, 1),
(12, 'ARDUINO UNO SMD rev3', 21, NULL, NULL, NULL, 1, 2),
(13, 'ASUS ZI 70I-PRO GAMING', 124.94, NULL, NULL, NULL, 1, 1),
(14, 'AXES PALIERS', 15, NULL, NULL, NULL, 1, 4),
(15, 'Batterie Li-Ion 3,7V 2900mA ( CGX )', 9, NULL, NULL, NULL, 1, 6),
(16, 'BOITIER carte a puce ', 10, NULL, NULL, NULL, 1, 70),
(17, 'BOITIER IEI IAI860 SHOEBOX', 25, NULL, NULL, NULL, 1, 1),
(18, 'BOITIER INFODIP SHOEBOX', 50, NULL, NULL, NULL, 1, 5),
(19, 'BOITIER MINI ITX', 45, NULL, NULL, NULL, 1, 2),
(20, 'BOITIER pour Whithbox CGX ', 35, NULL, NULL, NULL, 1, 14),
(21, 'BORNE A SERTIR MICROFIT FEMELLE 20-24AWG', 0.157, NULL, NULL, NULL, 1, 80),
(22, 'BRAS LONG MODULAIRE LINDY POUR ECRAN NOIR - B00MRN', 52.31, NULL, NULL, NULL, 1, 1),
(23, 'BROTHER ETIQUETTE RONDE (boite de1200)', 20.59, NULL, NULL, NULL, 1, 1),
(24, 'BUS PASSIF 14 SLOT IEI', 50, NULL, NULL, NULL, 1, 2),
(25, 'BUS PASSIF 8 SLOTS BP-PCI-8S (SAPRATIN)', 30, NULL, NULL, NULL, 1, 5),
(26, 'CABLE 1,5 Long : 100m vert/Jaune', 125, NULL, NULL, NULL, 1, 1),
(27, 'CABLE 15 FILS AWG 24', 1, NULL, NULL, NULL, 1, 50),
(28, 'cable alim sata', 10, NULL, NULL, NULL, 1, 90),
(29, 'CABLE HDMI 2,0/F 4K 60Hz 10m', 13.33, NULL, NULL, NULL, 1, 2),
(30, 'cable  sata', 6.89, NULL, NULL, NULL, 1, 300),
(31, 'CABLE 2 FILS', 85.5, NULL, NULL, NULL, 1, 1),
(32, 'Cable 32205-003800-300-RS  ', 4.8, NULL, NULL, NULL, 1, 3),
(33, 'Cable Y ps2', 2, NULL, NULL, NULL, 1, 70),
(34, 'CABLE HDMI - Panel 75', 3.23, NULL, NULL, NULL, 1, 4),
(35, 'Cable header 4', 0.83, NULL, NULL, NULL, 1, 200),
(36, 'Câble slot IDC26 D25 parallèle', 1.9, NULL, NULL, NULL, 1, 21),
(37, 'CABLE VERT 0,75 100M', 65, NULL, NULL, NULL, 1, 1),
(38, 'cable interne usb', 3.34, NULL, NULL, NULL, 1, 42),
(39, 'Câbles 2x USB pour rack 4U', 9, NULL, NULL, NULL, 1, 7),
(40, 'Cables secteur EUR 1,8 m', 1.15, NULL, NULL, NULL, 1, 10),
(41, 'Câbles secteur noir 2 m', 1.9, NULL, NULL, NULL, 1, 5),
(42, 'CAPOR METAL 75\'\'', 727, NULL, NULL, NULL, 1, 1),
(43, 'CAPOT SUB-D 9PT BLEU', 10.96, NULL, NULL, NULL, 1, 3),
(44, 'Carte backlight LED', 80, NULL, NULL, NULL, 1, 3),
(45, 'CARTE BC-199 USB 2,0 kit', 5, NULL, NULL, NULL, 1, 8),
(46, 'Carte électronique CGX Batch 2', 287.48, NULL, NULL, NULL, 1, 17),
(47, 'Carte électronique CGX Batch 3', 287.48, NULL, NULL, NULL, 1, 3),
(48, 'CARTE ELECTRONIQUE MEDUSA', 850, NULL, NULL, NULL, 1, 6),
(49, 'Carte GF STRIX GAMING 1080 - STOCK ITECA', 617.96, NULL, NULL, NULL, 1, 1),
(50, 'CARTE GRAPHIQUE GTX 790', 47, NULL, NULL, NULL, 1, 1),
(51, 'CARTE IEI IMBA-8550', 145, NULL, NULL, NULL, 1, 12),
(52, 'CARTE IEI KINO-DQM87', 631, NULL, NULL, NULL, 1, 1),
(53, 'CARTE IEI NANO D25501', 220, NULL, NULL, NULL, 1, 1),
(54, 'CARTE IEI PM-LX2-800W', 230, NULL, NULL, NULL, 1, 1),
(55, 'CARTE IEI WAFER D5252', 235, NULL, NULL, NULL, 1, 2),
(56, 'CARTE IEI WSB-9454-R40', 218, NULL, NULL, NULL, 1, 2),
(57, 'CARTE IEI WSB945GSE', 225, NULL, NULL, NULL, 1, 1),
(58, 'CARTE IEI WSB-G41A', 218, NULL, NULL, NULL, 1, 2),
(59, 'CARTE IEI WSB-Q870-i2', 298, NULL, NULL, NULL, 1, 1),
(60, 'CARTE INTEL DM2700', 72, NULL, NULL, NULL, 1, 2),
(61, 'CARTE INTEL DM2800', 85, NULL, NULL, NULL, 1, 2),
(62, 'CARTE MERE ASUS MAXIMUS', 195.12, NULL, NULL, NULL, 1, 1),
(63, 'carte leonardo avec embases', 17.44, NULL, NULL, NULL, 1, 2),
(64, 'CARTE MERE ASUS Z270 PRIME', 118.82, NULL, NULL, NULL, 1, 1),
(65, 'Carte mère Gigabyte MiniItx', 115, NULL, NULL, NULL, 1, 1),
(66, 'Carte power 75\" Stock Iteca', 55, NULL, NULL, NULL, 1, 1),
(67, 'Carte réseau ethernet (PPC75)', 13.56, NULL, NULL, NULL, 1, 1),
(68, 'Carte RGB v-by-one Stock ITECA', 65, NULL, NULL, NULL, 1, 1),
(69, 'Cartes RGB + LVDS pour écran AUO 17-19', 35, NULL, NULL, NULL, 1, 5),
(70, 'CDSOT236-0504LC composants SAC CGX', 1.53, NULL, NULL, NULL, 1, 6),
(71, 'CLAVIER KSK-6001UELX FR AZERTY USB', 19, NULL, NULL, NULL, 1, 0),
(72, 'CLAVIER KSK-6001UELX UK QWERTY USB', 35, NULL, NULL, NULL, 1, 0),
(73, 'CLAVIER LOGITECH WIRELESS MK270', 24.86, NULL, NULL, NULL, 1, 1),
(74, 'CLAVIER divers simple ', 6.5, NULL, NULL, NULL, 1, 10),
(75, 'CLAVIER USB FR', 6.5, NULL, NULL, NULL, 1, 1),
(76, 'CLE USB 16 GB', 3.85, NULL, NULL, NULL, 1, 1),
(77, 'CLE USB 32 GB', 5.45, NULL, NULL, NULL, 1, 0),
(78, 'COLONNETTES M3X10 plastic', 0.1, NULL, NULL, NULL, 1, 22),
(79, 'COLONNETTES M3X15plastic', 0.15, NULL, NULL, NULL, 1, 11),
(80, 'COLONNETTES M3X20 plastic', 0.1, NULL, NULL, NULL, 1, 42),
(81, 'COLONNETTES M4 X 10 Laiton', 0.22, NULL, NULL, NULL, 1, 100),
(82, 'CONVERTISSEUR VOITURE 12V/220V', 32, NULL, NULL, NULL, 1, 6),
(83, 'CONDENSATEUR CERAMIQUE SMD 0603 COG 100V', 0.042, NULL, NULL, NULL, 1, 40),
(84, 'CONDENSATEUR CERAMIQUE SMT', 0.052, NULL, NULL, NULL, 1, 95),
(85, 'CONDO CERAMIQUE COG 063 50V 18PF', 0.007, NULL, NULL, NULL, 1, 42),
(86, 'conductive Bench Floor Mat 1,2x0,6mx2mm', 68.54, NULL, NULL, NULL, 1, 1),
(87, 'CONTROLEUR USB DALLES', 18, NULL, NULL, NULL, 1, 98),
(88, 'Cooler kit LGA1150 65W for chassis 1U IEI', 34, NULL, NULL, NULL, 1, 2),
(89, 'cordon 1,8 fiche banane-pression 10 mm ', 12.76, NULL, NULL, NULL, 1, 1),
(90, 'Cordon CAT 6 RJ45 FTP NOIR 15m', 13.9, NULL, NULL, NULL, 1, 2),
(91, 'Cordon CAT 6 RJ45 FTP NOIR 20m', 16.9, NULL, NULL, NULL, 1, 2),
(92, 'Cordon CAT 6 RJ45 S-STP NOIR 2m', 2.5, NULL, NULL, NULL, 1, 10),
(93, 'Cordon CAT 6 RJ45 FTP NOIR 1m', 1.7, NULL, NULL, NULL, 1, 5),
(94, 'CORDON HDMI MALE/MINI HDMI MALE (1,5m)', 11.58, NULL, NULL, NULL, 1, 3),
(95, 'CORE I7-6700 3,40 GHZ', 265.87, NULL, NULL, NULL, 1, 1),
(96, 'Cosse à oeillet non isolée M3 0,5-1,5mm2', 0.292, NULL, NULL, NULL, 1, 85),
(97, 'Cosse à oeillet non isolée M4 0,5-1,5mm2', 0.31, NULL, NULL, NULL, 1, 37),
(98, 'Cosse à oeillet non isolée M5 0,5-1,5mm2', 0.047, NULL, NULL, NULL, 1, 65),
(99, 'Cosses à sertir Jaune Diam 4 mm', 0.77, NULL, NULL, NULL, 1, 17),
(100, 'Cosses à sertir Jaune Diam 5 mm', 0.77, NULL, NULL, NULL, 1, 5),
(101, 'CPU INTEL E7500', 75, NULL, NULL, NULL, 1, 1),
(102, 'CPU INTEL i3 -2200', 125, NULL, NULL, NULL, 1, 3),
(103, 'CPU INTEL i7-6700 ', 287.96, NULL, NULL, NULL, 1, 0),
(104, 'CR2032/1', 1.81, NULL, NULL, NULL, 1, 2),
(105, 'dalle 75\'\' lc 750ege-fhkm', 1350, NULL, NULL, NULL, 1, 1),
(106, 'DALLE TACTILE IR 47\" WIDE  USB', 150, NULL, NULL, NULL, 1, 1),
(107, 'DALLE TACTILE RESISTIVE 10,4\"', 100, NULL, NULL, NULL, 1, 2),
(108, 'Dalles TACTILE IR 21\" wide', 55, NULL, NULL, NULL, 1, 1),
(109, 'Dalles TACTILE Résistive 8\"', 20, NULL, NULL, NULL, 1, 3),
(110, 'Data Color Spyder 5 Pro S5P100', 132.4, NULL, NULL, NULL, 1, 1),
(111, 'DC power cable mount plug 1.4mm 1A  3m', 1.87, NULL, NULL, NULL, 1, 1),
(112, 'Diode TVS, SMBJ13A, composants SAC CGX', 0.56, NULL, NULL, NULL, 1, 10),
(113, 'Disque dur 1 OTB (KUKA)', 36.17, NULL, NULL, NULL, 1, 2),
(114, 'DOUBLE FACE MOUSSE PE/EVA NOIRE', 22.48, NULL, NULL, NULL, 1, 1),
(115, 'DUAL 2,5I SATA HDD/SSD TO3,5IN ACCS', 6.98, NULL, NULL, NULL, 1, 2),
(116, 'Dalle 55 pouce ', 1000, NULL, NULL, NULL, 1, 2),
(117, 'D-link 4 port usb 2,0 hub ', NULL, NULL, NULL, NULL, 1, 1),
(118, 'DVD SATA SAPPHIRE TECHNOLOGY 11166-08-20R ', 26.8, NULL, NULL, NULL, 1, 2),
(119, 'ECRAN LED AUO   8\"', 85, NULL, NULL, NULL, 1, 2),
(120, 'ECRAN LED AUO 15\"', 183, NULL, NULL, NULL, 1, 2),
(121, 'ECRAN LED AUO 17\"', 85, NULL, NULL, NULL, 1, 2),
(122, 'ECRAN TFT Chimei 10,4\" ', 85, NULL, NULL, NULL, 1, 2),
(123, 'ECRAN TFT Chimei 12\"1 ', 260, NULL, NULL, NULL, 1, 2),
(124, 'étagere PPC PAC-42GHW', 100, NULL, NULL, NULL, 1, 4),
(125, 'étagere PPC IAC-C860FA V1,1A', 100, NULL, NULL, NULL, 1, 5),
(126, 'Ecrous Hexagonaux en Acier zingué M3', 6.4, NULL, NULL, NULL, 1, 1),
(127, 'Ecrous Hexagonaux en Acier zingué M4', 6.55, NULL, NULL, NULL, 1, 1),
(128, 'Ecrous Hexagonaux en Acier zingué M5', 6.89, NULL, NULL, NULL, 1, 1),
(129, 'Ecrous Hexagonaux en Acier zingué M8', 7.28, NULL, NULL, NULL, 1, 1),
(130, 'EMBASE A ENTREE VERTICALE  3 CONTACTS', 0.8, NULL, NULL, NULL, 1, 8),
(131, 'embase de fixation mb3a noir 19x19mm ', 16.02, NULL, NULL, NULL, 1, 3),
(132, 'EMBASE  USBB', 2, NULL, NULL, NULL, 1, 70),
(133, 'EMBASE SPDIF', 16.78, NULL, NULL, NULL, 1, 3),
(134, 'ENTRETOISE INTERNE / EXTERNE M3x20mm  (sac de 50)', 13.23, NULL, NULL, NULL, 1, 0.6),
(135, 'Entretoise Interne / Interne M3 x 8 mm', 0.08, NULL, NULL, NULL, 1, 85),
(136, 'etiquettes adresse petites dk boite de 800', 16.6, NULL, NULL, NULL, 1, 1),
(137, 'FICHE 1718677 FA1-NDRP-PCB-9 PLUG, FAKRA VIOLET', 2.35, NULL, NULL, NULL, 1, 84),
(138, 'Fuse, 1206, fast acting, 12A, 24Vdc composants SAC', 2.91, NULL, NULL, NULL, 1, 10),
(139, 'gaine spirale 1,6mm ', 29.73, NULL, NULL, NULL, 1, 1),
(140, 'GAINE THERMO VRAC', 450, NULL, NULL, NULL, 1, 1),
(141, 'GH-414SRW-1,14S', 141, NULL, NULL, NULL, 1, 4),
(142, 'HD 250 GB SATA 3\'\' ½   ', 49.4, NULL, NULL, NULL, 1, 1),
(143, 'HD 500 GB SATA', 37.55, NULL, NULL, NULL, 1, 4),
(144, 'HD BARRACUDA 1TB SATA', 46.02, NULL, NULL, NULL, 1, 1),
(145, 'HD LOCK CITH LOCKING CONNECTOR 5m', 9.12, NULL, NULL, NULL, 1, 2),
(146, 'HD USB Sauvegarde 500 GB', 56.82, NULL, NULL, NULL, 1, 3),
(147, 'HEXAGON HEAD HIGH TENSILE BOLT,M8x120mm (sac de 25', 35.65, NULL, NULL, NULL, 1, 0.7),
(148, 'High Speed with Ethernet HDMI lead 5m', 17.39, NULL, NULL, NULL, 1, 1),
(149, 'INTEL 7260.HMWWB.R  WIFI MODULE ', 19.89, NULL, NULL, NULL, 1, 1),
(150, 'Interrupteur à bascule 2RT noir on-off', 2.9, NULL, NULL, NULL, 1, 4),
(151, 'Interrupteur ROCK. BIP. ON/OFF', 1.66, NULL, NULL, NULL, 1, 10),
(152, 'Interrupteur rocker 1NO noir, 0/-', 0.58, NULL, NULL, NULL, 1, 8),
(153, 'IP67 Cat 5e RJ45 panel PCB mount Jack', 35.25, NULL, NULL, NULL, 1, 1),
(154, 'IP67 usb 2.0 panel PCB mount Jack', 35.25, NULL, NULL, NULL, 1, 4),
(155, 'JOINT POUR MEDUSA KIT', 110, NULL, NULL, NULL, 1, 1),
(156, 'KINGSTON DT101G2/16GB ', 4.65, NULL, NULL, NULL, 1, 6),
(157, 'Kit ecran ', 50, NULL, NULL, NULL, 1, 5),
(158, 'Kit de montage NOIR 3\"½ -> 5\"¼ lecteur de disquett', 1.35, NULL, NULL, NULL, 1, 1),
(159, 'KIT HP pour les panels BST', 33.02, NULL, NULL, NULL, 1, 2),
(160, 'Kit VGA M190ETN01', 30, NULL, NULL, NULL, 1, 3),
(161, 'Lecteur disquettes ', 20, NULL, NULL, NULL, 1, 35),
(162, 'Logitech c270 webcam', NULL, NULL, NULL, NULL, 1, 1),
(163, 'LOGICIEL WINDOWS 7 PRO 64 BITS  ', 115, NULL, NULL, NULL, 1, 1),
(164, 'MAIN BOARD PE01 PINEVIEW', 250, NULL, NULL, NULL, 1, 2),
(165, 'MATERIEL EMBALLAGE RAJA', 350, NULL, NULL, NULL, 1, 1),
(166, 'Mémoire DDR3 8GB pour PanelPC USENDA', 37.56, NULL, NULL, NULL, 1, 4),
(167, 'Mémoire RAM DDR2 1 GB PC667 ', 17, NULL, NULL, NULL, 1, 8),
(168, 'Mémoire RAM DDR3 16 GB Panel', 37.5, NULL, NULL, NULL, 1, 6),
(169, 'MIKROBUS CLICK ADD-ON BOARD RS485 3,3V', 16.12, NULL, NULL, NULL, 1, 1),
(170, 'MINI PCI-E GIGGABIT ETHERNET CONTROLLER CARD ADAPT', 35, NULL, NULL, NULL, 1, 2),
(171, 'MODULE NV08-CMS', 45, NULL, NULL, NULL, 1, 1),
(172, 'MONITEUR LCD- IEI DM 170G', 85, NULL, NULL, NULL, 1, 1),
(173, 'MODULE TELIT HE863-EUD', 70, NULL, NULL, NULL, 1, 13),
(174, 'Nappe IDE', 2, NULL, NULL, NULL, 1, 110),
(175, 'NUF2042XV6T1G, USB UPSTR FTR IN composants SAC CGX', 14.72, NULL, NULL, NULL, 1, 1),
(176, 'Open office ', NULL, NULL, NULL, NULL, 1, 3),
(177, 'OSCILLATEUR A QUARTZ 11,0592 MHz XO 3,3V', 1.07, NULL, NULL, NULL, 1, 2),
(178, 'OSCILLATEUR A QUARTZ 14,31818 MHz XO 3,3V', 1.76, NULL, NULL, NULL, 1, 2),
(179, 'OSCILLATEUR A QUARTZ 20,000 MHz XO 3,3V', 2.21, NULL, NULL, NULL, 1, 1),
(180, 'OSCILLATEUR A QUARTZ 14,7456 MHz XO 3,3V', 2.21, NULL, NULL, NULL, 1, 2),
(181, 'Patin boitier auto collant X4', 0.7, NULL, NULL, NULL, 1, 60),
(182, 'Panel PC POINDUS VARIPPC-715 250 nits', 750, NULL, NULL, NULL, 1, 1),
(183, 'panel pc 15 pouce bst ', 85, NULL, NULL, NULL, 1, 1),
(184, 'panel pc 17 pouce tv/lcd monitor', 150, NULL, NULL, NULL, 1, 1),
(185, 'Panel PC POINDUS VARIPPC-715 300 nits', 715.385, NULL, NULL, NULL, 1, 1),
(186, 'Port parallèles ', NULL, NULL, NULL, NULL, 1, 5),
(187, 'PORTE-PILE BOUTONS,PCB, 24 mm', 2.17, NULL, NULL, NULL, 1, 4),
(188, 'Power switch bascul', 0.5, NULL, NULL, NULL, 1, 21),
(189, 'Power switch poussoir ', 2.7, NULL, NULL, NULL, 1, 850),
(190, 'QUARTZ SMD 8,000MHz 3,5x6mm', 1.398, NULL, NULL, NULL, 1, 8),
(191, 'GH-402 SRS , 4U Black,14slots ohne netzt ', 133, NULL, NULL, NULL, 1, 2),
(192, 'BC-199, USB 2,0 Kit mit kable f GH-xxx', 7, NULL, NULL, NULL, 1, 2),
(193, 'Rack 1 U noir JOY 620 clavier ', 100, NULL, NULL, NULL, 1, 1),
(194, 'Rack 3U Noir GH313 SRW', 117, NULL, NULL, NULL, 1, 1),
(195, 'RACK SSD noir', 14.06, NULL, NULL, NULL, 1, 1),
(196, 'RACK SSD SAPRATIN JJ-2225TL', 13.37, NULL, NULL, NULL, 1, 20),
(197, 'Radiateur/Ventilateur LGA 1150 très léger(200grs) ', 25, NULL, NULL, NULL, 1, 2),
(198, 'Rallonge usb 3,0 AA ', 8.25, NULL, NULL, NULL, 1, 1),
(199, 'RAM 1GB 667MHz', 17, NULL, NULL, NULL, 1, 4),
(200, 'RAM 2 GB 1066', 24.64, NULL, NULL, NULL, 1, 2),
(201, 'REPETEUR WIFI KIT DUO 1700 Mbit/s', 161.06, NULL, NULL, NULL, 1, 1),
(202, 'Réseau 5 diodes transils unidir. 6,4V composants S', 1.89, NULL, NULL, NULL, 1, 20),
(203, 'Ring Terminal VERSAKRIMP M3,5', 19.05, NULL, NULL, NULL, 1, 1),
(204, 'Riser Pcie 16x', 55, NULL, NULL, NULL, 1, 3),
(205, 'Rivet en plastique noir Ø4-4,1mm', 15.73, NULL, NULL, NULL, 1, 1),
(206, 'RONDELLE EVENTAIL M3', 2.96, NULL, NULL, NULL, 1, 150),
(207, 'RONDELLE INOX M2', 0.194, NULL, NULL, NULL, 1, 66),
(208, 'RONDELLE INOX M3', 0.19, NULL, NULL, NULL, 1, 52),
(209, 'RONDELLE INOX M8', 4.26, NULL, NULL, NULL, 1, 5),
(210, 'RONDELLE NYLON M3', 2.22, NULL, NULL, NULL, 1, 51),
(211, 'ROUND PLASTIC SPACERS 6/5', 10.92, NULL, NULL, NULL, 1, 1),
(212, 'ROULETTE 360', 19.76, NULL, NULL, NULL, 1, 30),
(213, 'ROUTEUR MOBILE WIRELESS N150 4G LTE ', 125, NULL, NULL, NULL, 1, 1),
(214, 'RUBAN 9088 50MM x 50M', 23.95, NULL, NULL, NULL, 1, 1),
(215, 'ruban polychloroprene 10m', 11.73, NULL, NULL, NULL, 1, 1),
(216, 'RUGGEDTECH ert-w3 docking station ', 65, NULL, NULL, NULL, 1, 1),
(217, 'resistances electroniques', 1, NULL, NULL, NULL, 1, 1000),
(218, 'RUBAN DOUBLE FACE 3M', 65, NULL, NULL, NULL, 1, 1),
(219, 'RH-40 ', 5, NULL, NULL, NULL, 1, 100),
(220, 'Rh-06F', 5, NULL, NULL, NULL, 1, 50),
(221, 'RUBAN POLYCHLOROPRENE 10M', 11.61, NULL, NULL, NULL, 1, 1),
(222, 'scie cloche p33', 13.67, NULL, NULL, NULL, 1, 1),
(223, 'SERINGUE D\'EPOXY CHARGE METAL,25ml', 8.34, NULL, NULL, NULL, 1, 1),
(224, 'Serre-câbles, 100 x 2,5, noir', 5.61, NULL, NULL, NULL, 1, 8),
(225, 'Serre-câbles, 200 X2,5 noir sac de 100', NULL, NULL, NULL, NULL, 1, 2),
(226, 'Serre-câbles, 300X5 blanc', 0.012, NULL, NULL, NULL, 1, 850),
(227, 'serre joint 50x150', 4.66, NULL, NULL, NULL, 1, 5),
(228, 'Socket 2 USB Boitier pc', 6.8, NULL, NULL, NULL, 1, 400),
(229, 'Socket 2 rs 232 boitier pc', 0.1, NULL, NULL, NULL, 1, 80),
(230, 'Sochet 1 rs 232 boitier pc ', 0.05, NULL, NULL, NULL, 1, 70),
(231, 'Socket ps2 chassis indus blanc', 2, NULL, NULL, NULL, 1, 200),
(232, 'Socket ps2 chassis indus noir ', 3, NULL, NULL, NULL, 1, 15),
(233, 'Souris LOGITEC USB', 4.35, NULL, NULL, NULL, 1, 2),
(234, 'SPILTTER HDMI 4K 1X2 PORT', 56.83, NULL, NULL, NULL, 1, 1),
(235, 'SSD KINGSTON 120GB ', 27.98, NULL, NULL, NULL, 1, 7),
(236, 'SSD 120 GB PANEL CLIMATS', 66.31, NULL, NULL, NULL, 1, 2),
(237, 'SSD  860 PRO sata  256 GB (KUKA)', 90.39, NULL, NULL, NULL, 1, 2),
(238, 'SSD KINGSTON SUV300S37A/120G ', 35.01, NULL, NULL, NULL, 1, 4),
(239, 'ST-133 ATA133 FAN, ALU, IDE, BEIGE', 9.76, NULL, NULL, NULL, 1, 6),
(240, 'STARTECH.COM PCISATA4R1 ', 34.51, NULL, NULL, NULL, 1, 1),
(241, 'Support de fixation pour SSD/HDD', 4.15, NULL, NULL, NULL, 1, 10),
(242, 'systéme dual lock 3550', 26.4, NULL, NULL, NULL, 1, 1),
(243, 'Tablette ARC-M116 11,6\" win 8-1', 534.69, NULL, NULL, NULL, 1, 1),
(244, 'TEK 4kDS-1 4K2K HDMI 1x2 SPLITTER DOWNSCALER/AUDIO', 285, NULL, NULL, NULL, 1, 2),
(245, 'TONER BLACK IMPRIMANTE HP MFP', 24, NULL, NULL, NULL, 1, 2),
(246, 'TONER BLACK IMPRIMANTE HP WIFI', 50.56, NULL, NULL, NULL, 1, 1),
(247, 'TONER BLACK 953XL', 30.55, NULL, NULL, NULL, 1, 2),
(248, 'TONER COULEUR IMPRIMANTE HP MFP', 25.88, NULL, NULL, NULL, 1, 10),
(249, 'TONER COULEUR IMPRIMANTE HP WIFI', 25.88, NULL, NULL, NULL, 1, 1),
(250, 'TONER COULEUR 953XL', 20.79, NULL, NULL, NULL, 1, 6),
(251, 'TPD2EUSB30ADRTR composants SAC CGX', 0.436, NULL, NULL, NULL, 1, 15),
(252, 'TUBE CARRE ACIER 40X40', 45, NULL, NULL, NULL, 1, 1),
(253, 'TUBE CARRE ALU 50X50', 75, NULL, NULL, NULL, 1, 1),
(254, 'TUBE ROND 30 MM LG 2,50M AVEC PALIERS', 75, NULL, NULL, NULL, 1, 2),
(255, 'usb to bluetooth 2,1 adaptateur class 2', 25, NULL, NULL, NULL, 1, 3),
(256, 'VENTILATEUR 4650N 240 VAC', 29.69, NULL, NULL, NULL, 1, 2),
(257, 'Ventilateur 90W pour chassis 1U', 39, NULL, NULL, NULL, 1, 5),
(258, 'VENTILO 1297 Cooler P199G Intel Pentium 775 - 1U', 19.9, NULL, NULL, NULL, 1, 8),
(259, 'VENTILO 1U 90W', 39, NULL, NULL, NULL, 1, 5),
(260, 'Ventilo toute taille ', 7.5, NULL, NULL, NULL, 1, 100),
(261, 'VENTILO K199G Intel 1155/1156 - 1U RoHS', 24, NULL, NULL, NULL, 1, 1),
(262, 'VENTILO P199G Intel Pentium 775 - 1U RoHS', NULL, NULL, NULL, NULL, 1, 3),
(263, 'VIA S TETE CYLINDRIQUE BOMBEE M4x12 (sac de 100)', 3.65, NULL, NULL, NULL, 1, 0.6),
(264, 'Vis à 6 pans creux BZP, M4x10', 43.29, NULL, NULL, NULL, 1, 1),
(265, 'VIS A RONDELLE M3', 0.03, NULL, NULL, NULL, 1, 185),
(266, 'VIS A RONDELLE M3X20', 0.01, NULL, NULL, NULL, 1, 110),
(267, 'VIS A RONDELLES M4X10', 0.007, NULL, NULL, NULL, 1, 250),
(268, 'vis à téte bombée M6 boite de 100', NULL, NULL, NULL, NULL, 1, 2),
(269, 'Vis à tête 6 pans  noire M8X80mm', 35, NULL, NULL, NULL, 1, 1),
(270, 'Vis à tête 6 pans creux noire M8x30mm', 21.44, NULL, NULL, NULL, 1, 1),
(271, 'Vis à tête 6 pans creux noire M8X45mm', 29.56, NULL, NULL, NULL, 1, 1),
(272, 'VIS A TETE 6 PANS CREUX NOIRE M8x70mm (boite de 50', 23.74, NULL, NULL, NULL, 1, 0.6),
(273, 'VIS À TÊTE BOMBÉ M2.5X16', 0.087, NULL, NULL, NULL, 1, 136),
(274, 'VIS ACIER M3X30 (Lot de 100)', 4.39, NULL, NULL, NULL, 1, 2),
(275, 'Vis acier noir M4x6mm', 4.68, NULL, NULL, NULL, 1, 1),
(276, 'VIS ACIER NOIRE M4X10 sachet de 100', 4.1, NULL, NULL, NULL, 1, 4),
(277, 'VIS INOX A4 M4X20', 0.1, NULL, NULL, NULL, 1, 148),
(278, 'VIS INOX M 3X20', 0.1, NULL, NULL, NULL, 1, 119),
(279, 'VIS M 2,5 X 20', 0.02, NULL, NULL, NULL, 1, 24),
(280, 'VIS M 3X25 INOX', 0.02, NULL, NULL, NULL, 1, 78),
(281, 'VIS M3 X 6 Noir sac de 100', 3.6, NULL, NULL, NULL, 1, 2),
(282, 'VIS M3 X 10 sac de 100', 3.6, NULL, NULL, NULL, 1, 2),
(283, 'VIS M 3X6', 0.11, NULL, NULL, NULL, 1, 77),
(284, 'VIS M3 X 20', 0.02, NULL, NULL, NULL, 1, 55),
(285, 'VIS M3 X 30', 0.03, NULL, NULL, NULL, 1, 46),
(286, 'VIS M3X10 zinguées', 2.16, NULL, NULL, NULL, 1, 1),
(287, 'VIS M4 X 6 zin sac de 100', 4.62, NULL, NULL, NULL, 1, 2),
(288, 'VIS M4 X 6  sac de 100', 4.8, NULL, NULL, NULL, 1, 2),
(289, 'VIS M4 X 10  sac de 100', 5.22, NULL, NULL, NULL, 1, 2),
(290, 'VIS M4 X 30', 0.04, NULL, NULL, NULL, 1, 37),
(291, 'VIS METAL TETE FRAISEE A FENTE M2,5X6MM', 3.17, NULL, NULL, NULL, 1, 1),
(292, 'Visserie divers Inox', 0.85, NULL, NULL, NULL, 1, 150),
(293, 'VISSERIE INOX M6', 0.299, NULL, NULL, NULL, 1, 85),
(294, 'Win 10', NULL, NULL, NULL, NULL, 1, 2),
(295, 'WIRE JUMPERS MALE TO FEMALE 10pcs', 2.78, NULL, NULL, NULL, 1, 2),
(296, 'WIRE JUMPERS MALE TO MALE 10pcs', 2.78, NULL, NULL, NULL, 1, 2),
(297, 'XH-2,5mm LOOSE PIECE CONTACT 28-22 AWG', 0.04, NULL, NULL, NULL, 1, 80),
(298, '', 204.343, NULL, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id_commande` int(11) NOT NULL,
  `date_commande` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_fournisseur` int(11) DEFAULT NULL,
  `ref_commande` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `fournisseur`
--

CREATE TABLE `fournisseur` (
  `id_fournisseur` int(11) NOT NULL,
  `nom_fournisseur` varchar(50) NOT NULL,
  `adresse_fournisseur` varchar(50) NOT NULL,
  `tel_fournisseur` int(11) NOT NULL,
  `ville_fournisseur` varchar(20) NOT NULL,
  `pays_fournisseur` varchar(20) NOT NULL,
  `etat_cli` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id_groupe` int(11) NOT NULL,
  `nom_groupe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ligne_commande`
--

CREATE TABLE `ligne_commande` (
  `id_ligne_commande` int(11) NOT NULL,
  `qt_articles` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_commande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ligne_historique`
--

CREATE TABLE `ligne_historique` (
  `id_ligne_historique` int(11) NOT NULL,
  `qt_articles` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_commande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id_article`),
  ADD KEY `relationRayonArticle` (`id_rayon`),
  ADD KEY `relationRackArticle` (`id_rack`),
  ADD KEY `relationProjetArticle` (`id_projet`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id_commande`),
  ADD KEY `relationFournisseurCommande` (`id_fournisseur`);

--
-- Index pour la table `fournisseur`
--
ALTER TABLE `fournisseur`
  ADD PRIMARY KEY (`id_fournisseur`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id_groupe`);

--
-- Index pour la table `ligne_commande`
--
ALTER TABLE `ligne_commande`
  ADD PRIMARY KEY (`id_ligne_commande`),
  ADD KEY `relationArticleLigne` (`id_article`),
  ADD KEY `relationCommandeLigne` (`id_commande`) USING BTREE;

--
-- Index pour la table `ligne_historique`
--
ALTER TABLE `ligne_historique`
  ADD PRIMARY KEY (`id_ligne_historique`),
  ADD KEY `relationArticleLigne` (`id_article`),
  ADD KEY `relationCommandeLigne` (`id_commande`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=299;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id_commande` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `fournisseur`
--
ALTER TABLE `fournisseur`
  MODIFY `id_fournisseur` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id_groupe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ligne_commande`
--
ALTER TABLE `ligne_commande`
  MODIFY `id_ligne_commande` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ligne_historique`
--
ALTER TABLE `ligne_historique`
  MODIFY `id_ligne_historique` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
