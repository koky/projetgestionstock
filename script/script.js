
// Récup input de la douchette
var test = document.getElementById("test");
var focus = true;
  console.log(focus);
// Ajout un listener "keydown" sur la fénètre, pour mettre le focus sur l'input avec la touche "$"
  window.addEventListener("keydown",function(event){
    //console.log("le KeyCode est: "+event.keyCode+" // le key est: "+event.key);
    if (focus == true) {
      if (event.keyCode==186) {
        test.focus();
      }
    }
  }, false);


// Cette fonction récupère la valeur de l'input et retire le focus de l'input de la douchette. Il remet la valeur de l'input vide.
function recupValeur(){
  if (event.keyCode==13) {
    save_valeur(test.value);
    test.value="";
    test.blur();
    focus = false;
    console.log(focus);
  }
}
// Cette fonction prend en paramètre la valeur de l'input douchette et l'envoie grâce à une requète httpRequest au script PHP
function save_valeur(x){

  $.ajax({
    url : 'php/produit.php',
    type : 'POST',
    data : {
      ref: x
    },
    dataType : 'json',
    success : function(code_html, statut){
    },
    error : function(resultat, statut, erreur){
      console.log("problème requète");
        console.log(resultat);
        console.log(statut);
        console.log(erreur);
    },
    complete : function(resultat, statut){
      var result = resultat.responseJSON;
      var warningNbr = document.getElementById("warningProduit");
      var nomProduit = document.getElementById("nom_produit");
      var prixProduit = document.getElementById("prix_produit");
      var totalStock = document.getElementById("totalStock");
      var nombreProduit = document.getElementById("nombre_produit");
      var pasBdd = document.getElementById("pasBdd");
      var code = document.getElementById("codeBar");
      code.value=x;
      warningNbr.style.display="none";
        if (result.result == true) {
          nombreProduit.value=1;
          pasBdd.style.display="none";
          totalStock.value = result.produit['quantite_total'];
          nomProduit.value = result.produit["nom_article"];
          prixProduit.value = result.produit["prix_unitaire"];
        }else if (result.result == false){
          nombreProduit.value=1;
          totalStock.value = 0;
          pasBdd.style.display="inline-block";
          nomProduit.value = "";
          prixProduit.value = "";
          nomProduit.placeholder = "Nom produit";
          prixProduit.placeholder = "prix produit";
        }else {
          console.log("WTFFFFFFFFFFFFFF");
        }
      }
    });
  var modal = document.getElementsByClassName("modal");
  var span = document.getElementsByClassName("close")[0];
  span.onclick = function() {
    modal[0].style.display = "none";
    focus = true;
      console.log(focus);
  }
  window.onclick = function(event) {
    if (event.target == modal[0]) {
      modal[0].style.display = "none";
      focus = true;
        console.log(focus);
    }
  }
  modal[0].style.display="block";
}
test.addEventListener("keydown",recupValeur, false);

//
//
//
//
//
//
//
//
//
//

var boutonAjoutStock=document.getElementById("ajouterStock");
var boutonSuppStock=document.getElementById("suppStock");
var code = document.getElementById("test");


boutonAjoutStock.addEventListener("click", ajoutStock, false)
boutonSuppStock.addEventListener("click", suppStock, false)
function suppStock(){
  focus = true;
  var code = document.getElementById("codeBar");
  var produitTotal = document.getElementById("totalStock");
  var nomProduitCode = document.getElementById("nom_produit");
  var prixProduitCode = document.getElementById("prix_produit");
  var nombreProduitCode = document.getElementById("nombre_produit");
  console.log(code.value);
  $.ajax({
    url : 'php/suppresion.php',
    type : 'POST',
    data : {
      produitTotal:produitTotal.value,
      codeBar: code.value,
      nom_produit: nomProduitCode.value,
      prix_produit: prixProduitCode.value,
      nombre_produit: nombreProduitCode.value
    },
    dataType : 'json',
    success : function(code_html, statut){
    },
    error : function(resultat, statut, erreur){
      console.log("problème requète");
        console.log(resultat);
        console.log(statut);
        console.log(erreur);
    },
    complete : function(resultat, statut){
      var result = resultat.responseJSON;

      console.log(result['content']);
        if (result.result == true) {
          var modal = document.getElementsByClassName("modal");
          modal[0].style.display = "none";
        }else if (result.result == false){
          var warningNbr = document.getElementById("warningProduit");
          warningNbr.style.display="inline-block";
          warningNbr.style.color="red";
          console.log("Attention, pas assez de stock");
        }else {
          console.log("WTFFFFFFFFFFFFFF");
        }
    }
  });


}
function ajoutStock(){
    focus = true;
  var code = document.getElementById("codeBar");
  var nomProduitCode = document.getElementById("nom_produit");
  var prixProduitCode = document.getElementById("prix_produit");
  var nombreProduitCode = document.getElementById("nombre_produit");
  var refCommande = document.getElementById("ref_commande");
  console.log(code.value);
  $.ajax({
    url : 'php/ajout.php',
    type : 'POST',
    data : {
      ref_commande: refCommande.value,
      codeBar: code.value,
      nom_produit: nomProduitCode.value,
      prix_produit: prixProduitCode.value,
      nombre_produit: nombreProduitCode.value
    },
    dataType : 'json',
    success : function(code_html, statut){
    },
    error : function(resultat, statut, erreur){
      console.log("problème requète");
        console.log(resultat);
        console.log(statut);
        console.log(erreur);
    },
    complete : function(resultat, statut){
      var result = resultat.responseJSON;
      console.log(result['content']);
        if (result.result == true) {

        }else if (result.result == false){

        }else {
          console.log("WTFFFFFFFFFFFFFF");
        }
    }
  });
  var modal = document.getElementsByClassName("modal");
  modal[0].style.display = "none";
}
