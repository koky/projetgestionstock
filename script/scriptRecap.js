// METTRE EN ROUGE QUAND IL Y A DES CASES DU TABLEAU NON DEFINIS
var table=document.getElementById("tableau");
var sousTable = table.children;
for (var i = 0; i < sousTable.length; i++) {
  for (var y = 0; y < sousTable[i].children.length; y++) {

    if (sousTable[i].children[y].textContent =="A DEFINIR") {
        sousTable[i].style.backgroundColor="red";
        sousTable[i].style.color="white";
    }
  }
}
//==================================================

// FONCTION QUI EST APPELE QUAND ON CLIC SUR UNE LIGNE DU TABLEAU
// REMPLIE LES CASES NON DEFIS PAR "A DEFINIR"
// OUVRE LA MODAL EST REMPLIE LES CHAMPS
// GERE LA FERMETURE DE LA MODAL
function msgbox(eltDom){

  var modal = document.getElementsByClassName("modal");
  var span = document.getElementsByClassName("close")[0];
  modal[0].style.display="block";
  var designation = eltDom.children[0].textContent;
  if (designation =="A DEFINIR") {
    designation = "NULL";
  }
  var quantite = eltDom.children[1].textContent;
  if (quantite =="A DEFINIR") {
    quantite = "NULL";
  }
  var prixUnitaire = eltDom.children[2].textContent;
  if (prixUnitaire =="A DEFINIR") {
    prixUnitaire = "NULL";
  }
  var prixUnitaire = eltDom.children[4].textContent;
  if (prixUnitaire =="A DEFINIR") {
    prixUnitaire = "NULL";
  }
  var id = eltDom.children[5].value;
  console.log(designation+" || "+quantite+" || "+prixUnitaire+" || "+id);

  // ======== AJAX utilisation JQUERY ==========
  $.ajax({
    url : 'php/produitById.php',
    type : 'POST',
    data : {
      ref: id
    },
    dataType : 'json',
    success : function(code_html, statut){
    },
    error : function(resultat, statut, erreur){
      console.log("problème requète");
        console.log(resultat);
        console.log(statut);
        console.log(erreur);
    },
    complete : function(resultat, statut){
      var result = resultat.responseJSON;
      console.log(result);

      var nomProduit = document.getElementById("nom_produit");
      var prixProduit = document.getElementById("prix_produit");
      var totalStock = document.getElementById("totalStock");
      var refProduit = document.getElementById("codeBar");
      var id =document.getElementById("id");

        if (result.result == true) {
          totalStock.value = result.produit['quantite_total'];
          nomProduit.value = result.produit["nom_article"];
          prixProduit.value = result.produit["prix_unitaire"];
          refProduit.value = result.produit["ref_produit"];
          id.value =result.produit["id_article"];
          console.log( result.produit["prix_unitaire"]);
        }else if (result.result == false){
          alert("Il y a un problème, je pense vous devriez refresh la page");
        }else {
          console.log("WTFFFFFFFFFFFFFF");
        }
      }
    });
  // Gestion fermeture MODAL
  span.onclick = function() {
    modal[0].style.display = "none";

  }
  window.onclick = function(event) {
    if (event.target == modal[0]) {
      modal[0].style.display = "none";
    }
  }
}

//=============================================================
// SAUVGARDER LES MODIFICATIONS
var boutonSaveModif= document.getElementById("save");
boutonSaveModif.addEventListener("click", saveModif, false);
function saveModif(){
  var code = document.getElementById("codeBar");
  var nomProduitCode = document.getElementById("nom_produit");
  var prixProduitCode = document.getElementById("prix_produit");
  var nombreProduitCode = document.getElementById("totalStock");
  var idProduit = document.getElementById("id");
  var modal = document.getElementsByClassName("modal");

  // ========== AJAX avec Jquery =============

  $.ajax({
    url : 'php/modification.php',
    type : 'POST',
    data : {
      codeBar: code.value,
      nom_produit: nomProduitCode.value,
      prix_produit: prixProduitCode.value,
      nombre_produit: nombreProduitCode.value,
      id_article: idProduit.value
    },
    dataType : 'json',
    success : function(code_html, statut){
    },
    error : function(resultat, statut, erreur){
      console.log("problème requète");
        console.log(resultat);
        console.log(statut);
        console.log(erreur);
    },
    complete : function(resultat, statut){
      var result = resultat.responseJSON;


        if (result.result == true) {
          modal[0].style.display = "none";
          var inputId = document.getElementsByClassName("id");
          for (var i = 0; i < inputId.length; i++) {
            if (inputId[i].value == idProduit.value) {
              var parent = inputId[i].parentNode;
              console.log(result.content[0]);
              parent.children[0].textContent =result.content["nom_article"];
              parent.children[1].textContent =result.content['quantite_total'];
              parent.children[2].textContent =result.content['prix_unitaire'];
              parent.children[3].textContent =result.content['prix_unitaire']*result.content['quantite_total'];
              parent.children[4].textContent= result.content['ref_produit'];
            }
          }
        for (var i = 0; i < 5; i++) {
          if (parent.children[i].textContent == "") {
              parent.children[i].textContent="A DEFINIR";
          }
          if (parent.children[i].textContent =="A DEFINIR"){
              parent.style.backgroundColor="red";
              parent.style.color="white";
          }else {
              parent.style.backgroundColor="white";
              parent.style.color="black";
          }
        }

        }else if (result.result == false){

        }else {

        }
    }
  });
}
function ajoutStock(){
  var modal = document.getElementsByClassName("modal");
  var span = document.getElementsByClassName("close")[1];

  var boutonAjoutGroupe= document.getElementById("ajoutGroupe");
  boutonAjoutGroupe.addEventListener("click", enregistrerGroup ,false)

  modal[1].style.display="block";
  // Gestion fermeture MODAL
  span.onclick = function() {
    modal[1].style.display = "none";

  }
  window.onclick = function(event) {
    if (event.target == modal[1]) {
      modal[1].style.display = "none";
    }
  }
}
function enregistrerGroup(){
  var modal = document.getElementsByClassName("modal");
  var nomGroupe = document.getElementById("nom_stock");
  console.log(nomGroupe.value);
  if (nomGroupe.value =="") {

  }else {
      modal[1].style.display = "none";
    $.ajax({
      url : 'php/ajoutGroupe.php',
      type : 'POST',
      data : {
          nomGroupe: nomGroupe.value,
      },
      dataType : 'json',
      success : function(code_html, statut){
      },
      error : function(resultat, statut, erreur){
        console.log("problème requète");
          console.log(resultat);
          console.log(statut);
          console.log(erreur);
      },
      complete : function(resultat, statut){
        var result = resultat.responseJSON;
        console.log(result.content);
        var listeGroupe = document.getElementById("groupe");
        while (listeGroupe.firstChild) {
          listeGroupe.removeChild(listeGroupe.firstChild);
        }
        for (var i = 0; i < listeGroupe.length; i++) {
          listeGroupe[i];
        }
      }
    });
  }
}


var btnSearch = document.getElementById("buttonSearch");
btnSearch.addEventListener("click", search, false);

function search(){
  var valueSearch = document.getElementById("recherche").value;
  if (!valueSearch) {
    alert("Rien dans le champ");
  }else {
    console.log(valueSearch);
    $.ajax({
      url : 'php/search.php',
      type : 'POST',
      data : {
          valueSearch: valueSearch,
      },
      dataType : 'json',
      success : function(code_html, statut){
      },
      error : function(resultat, statut, erreur){
        console.log("problème requète");
          console.log(resultat);
          console.log(statut);
          console.log(erreur);
      },
      complete : function(resultat, statut){

        if (document.getElementById("displaySearch")) {
          var node = document.getElementById("displaySearch");
          if (node.parentNode) {
            node.parentNode.removeChild(node);
          }
        }
        var result = resultat.responseJSON;
        var defaultTab = document.getElementById("tableau");
        var tabContent = document.getElementById("tabContent");
        defaultTab.style.display="none";
        var searchTab = document.createElement("tbody");
        searchTab.setAttribute("id", "displaySearch");
        tabContent.appendChild(searchTab);
        console.log(result.content);
        for (var i = 0; i < result.content.length; i++) {
          var ligneSearchTab = document.createElement("tr");
          searchTab.appendChild(ligneSearchTab);
          ligneSearchTab.setAttribute("onclick", "msgbox(this)");
          for (var y = 0; y < 6; y++) {

            if (y==0) {
              var colomnSearchTab = document.createElement("td");
              ligneSearchTab.appendChild(colomnSearchTab);
              colomnSearchTab.textContent=result.content[i]["nom_article"];
            }else if (y==1) {
              var colomnSearchTab = document.createElement("td");
              ligneSearchTab.appendChild(colomnSearchTab);
              colomnSearchTab.textContent=result.content[i]["quantite_total"];
            }else if (y==2) {
              var colomnSearchTab = document.createElement("td");
              ligneSearchTab.appendChild(colomnSearchTab);
              colomnSearchTab.textContent=result.content[i]["prix_unitaire"];
            }else if (y==3) {
              var colomnSearchTab = document.createElement("td");
              ligneSearchTab.appendChild(colomnSearchTab);
              colomnSearchTab.textContent=result.content[i]["prix_unitaire"]*result.content[i]['quantite_total'];

            }else if (y==4) {
              var colomnSearchTab = document.createElement("td");
              ligneSearchTab.appendChild(colomnSearchTab);
              colomnSearchTab.textContent=result.content[i]["ref_produit"];
            }else if (y==5) {
              var colomnSearchTab = document.createElement("input");
              colomnSearchTab.setAttribute("class", "id");
              colomnSearchTab.setAttribute("type", "hidden");
              colomnSearchTab.setAttribute("value", result.content[i]["id_article"]);
              ligneSearchTab.appendChild(colomnSearchTab);
            }else {
              alert("bug");
            }
          }
        }

      }
    });
  }

}
