<?php
include 'connexion.php';
header( 'content-type: text/html; charset=utf-8' );
$row = 1;
if (($handle = fopen("../fichiers/stocks.csv", "r")) !== FALSE):
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE):
        $num = count($data);
        $row++;
        if ($row>13) {
          $insertProduit = $bdd->prepare("INSERT INTO article (nom_article, prix_unitaire, quantite_total) VALUES (?, ?, ?)");
          $insertProduit->bindParam(1, $nom_article);
          $insertProduit->bindParam(2, $prix_unitaire);
          $insertProduit->bindParam(3, $quantite_total);
          echo "<br /><br /><p> $num champs à la ligne $row:</p>";


          for ($c=0; $c < $num; $c++):
               echo $data[$c] . " | ";
               if ($c == 0) {
                  $nom_article = $data[$c];
                  echo $data[$c];
               } elseif ($c == 1) {
                  $quantite_total = str_replace(",", ".", $data[$c]);
               } elseif ($c == 3) {
                 if ($data[$c]=="") {
                   $prix_unitaire = NULL;
                 }else {
                  $prix_unitaire = str_replace(",", ".", $data[$c]);

                 }

               }
          endfor;
          $insertProduit->execute();
        }

    endwhile;
    fclose($handle);
endif;
 ?>
