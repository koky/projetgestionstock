<?php
include 'connexion.php';
$output=[];

if ( isset($_POST['valueSearch'])) {
  $_POST['valueSearch'] = htmlspecialchars($_POST['valueSearch']); //pour sécuriser le formulaire contre les failles html
  $valueSearch = $_POST["valueSearch"];
  $valueSearch = trim($valueSearch); //pour supprimer les espaces dans la requête de l'internaute
  $valueSearch = strip_tags($valueSearch); //pour supprimer les balises html dans la requête

  $select_terme = $bdd->prepare("SELECT * FROM article WHERE nom_article LIKE ? ");
  $select_terme->execute(array("%".$valueSearch."%"));
  $listTerme = $select_terme->fetchAll(PDO::FETCH_ASSOC);

  $output['result']=false;
  $output['content']=$listTerme;

}else {
  $output['result']=false;
  $output['content']="aucune valeur";
}


  echo json_encode($output);
?>
