<?php
include 'connexion.php';
$output=[];
  // =============================
  // On récuper l'id de l'article

  // if ($leProduit['id_article']) {
    $updateArticle = $bdd->prepare("UPDATE `article` SET `ref_produit` = ?, `nom_article` = ?, `prix_unitaire` = ?, `quantite_total`= ? WHERE `id_article`= ?");
    $updateArticle->bindParam(1, $codeBar);
    $updateArticle->bindParam(2, $nomProduit);
    $updateArticle->bindParam(3, $prixProduit);
    $updateArticle->bindParam(4, $quantiteProduit);
    $updateArticle->bindParam(5, $id_article);

    $codeBar = $_POST['codeBar'];
    $nomProduit = $_POST['nom_produit'];
    $prixProduit = $_POST['prix_produit'];
    $quantiteProduit = $_POST['nombre_produit'];
    $id_article = $_POST['id_article'];


    $updateArticle->execute();

    $recupProduit = $bdd->prepare("SELECT * FROM `article` WHERE `id_article`= :id");
    $recupProduit->execute(array(
      'id' => $_POST['id_article']));
    $leProduit = $recupProduit->fetch(PDO::FETCH_ASSOC);


    $output['result']=true;
    $output['content']=$leProduit;
    $output['modifier']= "C'est modifier et l'article est séléctionné";
  // } elseif (!$leProduit['id_article']) {
    // $output['result']=false;
    // $output['content']="le produit n'existe pas";
  // }else {
    // $output['result']=false;
    // $output['content']="C'est pas normal que je m'affiche";

  echo json_encode($output);
?>
