<?php
include 'connexion.php';
include 'commande.php';

$output=[];

// On regarde si l'article existe
// if (isset($_POST['ajouter'])) {
  $recupProduit = $bdd->prepare("SELECT `ref_produit`, `id_article`, `quantite_total` FROM `article` WHERE `ref_produit`= :nom");
  $recupProduit->execute(array(
    'nom' => $_POST['codeBar']));
  $leProduit = $recupProduit->fetch(PDO::FETCH_ASSOC);
  // =============================
  // Si l'article existe, on insère dans la table ligne_commande
  if ($leProduit['ref_produit']) {
    // INSERTION LIGNE COMMANDE
    $insertLigneCommande = $bdd->prepare("INSERT INTO ligne_commande (qt_articles, id_article, id_commande) VALUES (?, ?, ?)");
    $insertLigneCommande->bindParam(1, $qt_articles);
    $insertLigneCommande->bindParam(2, $id_article);
    $insertLigneCommande->bindParam(3, $id_commande);
    $qt_articles = $_POST['nombre_produit'];
    $id_article = intval($leProduit['id_article']);
    $id_commande = $commande['id_commande'];
    $insertLigneCommande->execute();
    //UPDATE QUANTITE_TOTAL
    $updateArticle = $bdd->prepare("UPDATE `article` SET `quantite_total` = ? WHERE `id_article`= ?");
    $updateArticle->bindParam(1, $qt_total);
    $updateArticle->bindParam(2, $id_article);

    $qt_total = $leProduit['quantite_total']+$_POST['nombre_produit'];
    $id_article = intval($leProduit['id_article']);
    $updateArticle->execute();
    $output['result']=true;
    $output['content']="Insertion ligne_commande ok. Update article ok";
    // =============================
    // Si l'article existe pas, on insère d'abord l'article dans la table article
  } elseif (!$leProduit['ref_produit']) {
    $insertProduit = $bdd->prepare("INSERT INTO article (nom_article, prix_unitaire, ref_produit, id_rack, id_rayon, id_projet) VALUES (?, ?, ?, ?, ?, ?)");
    $insertProduit->bindParam(1, $nom_article);
    $insertProduit->bindParam(2, $prix_unitaire);
    $insertProduit->bindParam(3, $ref_produit);
    $insertProduit->bindParam(4, $id_rack);
    $insertProduit->bindParam(5, $id_rayon);
    $insertProduit->bindParam(6, $id_projet);

    $nom_article = $_POST['nom_produit'];
    $prix_unitaire = $_POST['prix_produit'];
    $ref_produit = $_POST['codeBar'];
    $id_rack = 25;
    $id_rayon = 6;
    $id_projet = 5;
    $insertProduit->execute();
    // =============================
    // On récupère l'id de l'article que l'on vient d'ajouter
    $recupNewProduit = $bdd->prepare("SELECT `ref_produit`, `id_article`, `quantite_total` FROM `article` WHERE `ref_produit`= :nom");
    $recupNewProduit->execute(array(
      'nom' => $_POST['codeBar']));
    $newProduit = $recupNewProduit->fetch(PDO::FETCH_ASSOC);
    // =============================
    // Puis on insère enfin dans la table ligne_de_commande
    $insertLigneCommande = $bdd->prepare("INSERT INTO ligne_commande (qt_articles, id_article, id_commande) VALUES (?, ?, ?)");
    $insertLigneCommande->bindParam(1, $qt_articles);
    $insertLigneCommande->bindParam(2, $id_article);
    $insertLigneCommande->bindParam(3, $id_commande);
    $qt_articles = $_POST['nombre_produit'];
    $id_article = intval($newProduit['id_article']);
    $id_commande = $commande['id_commande'];
    $insertLigneCommande->execute();
    //UPDATE QUANTITE_TOTAL
    $updateArticle = $bdd->prepare("UPDATE `article` SET `quantite_total` = ? WHERE `id_article`= ?");
    $updateArticle->bindParam(1, $qt_total);
    $updateArticle->bindParam(2, $id_article);

    $qt_total = $newProduit['quantite_total']+$_POST['nombre_produit'];
    $id_article = intval($newProduit['id_article']);
    $updateArticle->execute();
    $output['result']=true;
    $output['content']="le produit a été ajouté en base et la ligne de commande aussi";
  //header('Location: ../index.php');
}else {
  $output['result']=false;
  $output['content']="Mmmh c'est pas normal";
}

echo json_encode($output);
?>
