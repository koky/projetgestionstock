<?php
$commandeRef=$_POST['ref_commande'];
$recupCommande = $bdd->prepare('SELECT * FROM `commande`
WHERE ref_commande = ?');
$recupCommande->execute(array($_POST['ref_commande']));
$commande = $recupCommande->fetch(PDO::FETCH_ASSOC);

if (!$commande) {
  //INSERT COMMANDE
  $insertCommande = $bdd->prepare("INSERT INTO commande (ref_commande) VALUES (?)");
  $insertCommande->bindParam(1, $ref_commande);
  $ref_commande = $commandeRef;
  $insertCommande->execute();
  //RECUP COMMANDE
  $recupCommande = $bdd->prepare('SELECT * FROM `commande`
  WHERE ref_commande = ?');
  $recupCommande->execute(array($_POST['ref_commande']));
  $commande = $recupCommande->fetch(PDO::FETCH_ASSOC);
}

?>
