<?php
include 'connexion.php';
$output=[];
  // =============================
  // On récuper l'id de l'article
  $recupProduit = $bdd->prepare("SELECT `ref_produit`, `id_article`, `quantite_total` FROM `article` WHERE `ref_produit`= :nom");
  $recupProduit->execute(array(
    'nom' => $_POST['codeBar']));
  $leProduit = $recupProduit->fetch(PDO::FETCH_ASSOC);

  if ($leProduit['ref_produit']) {
    if ( $_POST['nombre_produit']>$leProduit['quantite_total']) {
      $output['result']=false;
      $output['content']="il y en a pas assez";
    }elseif ( $_POST['nombre_produit']<=$leProduit['quantite_total']) {
      //INSERTION LIGNE_HISTORIQUE
      $insertLigneHisto = $bdd->prepare("INSERT INTO ligne_historique (qt_articles, id_article, id_commande) VALUES (?, ?, ?)");
      $insertLigneHisto->bindParam(1, $qt_articles);
      $insertLigneHisto->bindParam(2, $id_article);
      $insertLigneHisto->bindParam(3, $id_commande);

      $qt_articles = $_POST['nombre_produit'];
      $id_article = intval($leProduit['id_article']);
      $id_commande = 6;
      $insertLigneHisto->execute();
      //UPDATE QUANTITE_TOTAL
      $updateArticle = $bdd->prepare("UPDATE `article` SET `quantite_total` = ? WHERE `id_article`= ?");
      $updateArticle->bindParam(1, $qt_total);
      $updateArticle->bindParam(2, $id_article);

      $qt_total = $leProduit['quantite_total']-$_POST['nombre_produit'];
      $id_article = intval($leProduit['id_article']);
      $updateArticle->execute();
      $output['result']=true;
      $output['content']="c'est retiré et article à été mis à jour";

    }else{
      $output['result']=false;
      $output['content']="C'est pas normal";
    }
  } elseif (!$leProduit['ref_produit']) {
    $output['result']=false;
    $output['content']="coucou,il y en plus";
  }else {
    $output['result']=false;
    $output['content']="C'est pas normal que je m'affiche";
  }
  echo json_encode($output);
?>
