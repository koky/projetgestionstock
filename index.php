<!DOCTYPE html>
<?php
    include 'php/connexion.php';
 ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Gestion Stock</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <header>
      <nav>
          <input type="search" id="test" name="ref" value="" placeholder="Scanner votre article">
      </nav>
    </header>
    <article class="navigation_left">
      <ul>
        <li><a href="Tableau_recap.php">Voir tableau récap</a></li>
      </ul>
    </article>
    <div class="modal">
      <div class="modal-content">
        <!-- <form  action="php/ajoutSuppresion.php" method="post"> -->
          <h2 id="pasBdd">Cette article n'est pas dans la base de donnée</h2>
          <span class="close">&#10006;</span>
          <div class="formulaire">
            <div class="commande">
              <h2>Commande</h2>
              <label for="ref_commande">Référence commande</label>
              <input type="text" name="ref_commande" id="ref_commande" value="">
              <label for="date_commande">Date</label>
              <input type="text" name="date_commande" id="date_commande" value="">
              <textarea id="note_commande" name="note_commande" placeholder="Commentaire ..."></textarea>
            </div>
            <div class="article">
              <label for="Nom_produit">Nom du produit</label>
              <input type="text" name="nom_produit" id="nom_produit" value="">
              <label for="prix_produit">Prix unitaire</label>
              <input type="text" name="prix_produit" id="prix_produit" value="">
              <label for="totalStock">totalStock</label>
              <input type="text" name="totalStock" id="totalStock" value="">
              <label for="codeBar">Référence Article</label>
              <input type="text" name="codeBar" id="codeBar" value="">
              <br>
              <br>
              <label for="nombre_produit">Nombre produit</label>
              <span id="warningProduit">ATTENTION IL Y EN A PAS ASSEZ</span>
              <input type="number" name="nombre_produit" id="nombre_produit" value="1">
              <input type="button" id="ajouterStock" name="ajouter" value="Ajouter au stock">
              <input type="button" id="suppStock" name="supprimer" value="Enlever au stock">
            </div>
          </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="script/script.js"></script>
  </body>
</html>
